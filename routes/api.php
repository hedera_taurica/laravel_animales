<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/animales', function() {
    $animales = ['gato', 'perro', 'pollo'];
    $animal = $animales[array_rand($animales)];

    $logLevels = ['info', 'warning', 'error', 'debug'];
    $randomLogLevel = $logLevels[array_rand($logLevels)];
    Log::{$randomLogLevel}('Animal seleccionado: ' . $animal);

    return response()->json(['animal' => $animal]);
});
